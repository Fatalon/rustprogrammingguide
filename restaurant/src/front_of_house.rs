pub mod hosting;
// ; instead of {} indicates to rust to search inside the respective file
// the mod keyword declares modules, and Rust looks in a file with the same name as the module for the code that goes into that module.
