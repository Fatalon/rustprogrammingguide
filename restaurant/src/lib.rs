// crate
//  └── front_of_house
//      ├── hosting
//      │   ├── add_to_waitlist
//      │   └── seat_at_table
//      └── serving
//          ├── take_order
//          ├── serve_order
//          └── take_payment

// mod front_of_house {
//     mod hosting {
//         fn add_to_waitlist() {}

//         fn seat_at_table() {}
//     }

//     mod serving {
//         fn take_order() {}

//         fn serve_order() {}

//         fn take_payment() {}
//     }
// }

mod front_of_house; //moved into own file

use crate::front_of_house::hosting;

// or alternatively with relative path
// use self::front_of_house::hosting;

// typically specify parent module for functions and whole path for structs, enums, and other items like:
// use std::collections::HashMap;

// rename types via as statement
// use std::io::Result as IoResult;

// re-exporting - allows other code to call 'hosting::add_to_waitlist();' without need to know about front_of_house, back_of_house stuff
// useful for libraries to restructure public API
// pub use crate::front_of_house::hosting;

pub fn eat_at_restaurant() {
    // Absolute path
    crate::front_of_house::hosting::add_to_waitlist(); // absolut path, preferred, more likely that definition and implementation will move seperately

    // Relative path
    front_of_house::hosting::add_to_waitlist(); // relative path used, when more likely that code will be moved togehter

    // Order a breakfast in the summer with Rye toast
    let mut meal = back_of_house::Breakfast::summer("Rye");
    // Change our mind about what bread we'd like
    meal.toast = String::from("Wheat");
    println!("I'd like {} toast please", meal.toast);

    // The next line won't compile if we uncomment it; we're not allowed
    // to see or modify the seasonal fruit that comes with the meal
    // meal.seasonal_fruit = String::from("blueberries");

    let _order1 = back_of_house::Appetizer::Soup;
    let _order2 = back_of_house::Appetizer::Salad;

    //use usage - brings path into scope, like a symbolic link, such that hosting is now inside root crate directory
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
}

fn _serve_order() {}

mod back_of_house {
    fn _fix_incorrect_order() {
        _cook_order();
        super::_serve_order(); //super used when relative relationship will stay the same (server_order stored in parent module of back_of_house)
    }

    fn _cook_order() {}

    // only struct and inner pub fields are public, requires public constructor, if some fields are not public
    pub struct Breakfast {
        pub toast: String,
        _seasonal_fruit: String,
    }
    impl Breakfast {
        pub fn summer(toast: &str) -> Breakfast {
            Breakfast {
                toast: String::from(toast),
                _seasonal_fruit: String::from("peaches"),
            }
        }
    }

    // all public
    pub enum Appetizer {
        Soup,
        Salad,
    }
}

//use std::{cmp::Ordering, io};
// similar too  use
// std::cmp::Ordering;
// use std::io;
// or use std::io::{self, Write};
// for
// use std::io;
// use std::io::Write;

// global (all modules part of a path)
// use std::collections::*;
