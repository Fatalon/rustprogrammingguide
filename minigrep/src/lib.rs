use std::env;
use std::error::Error;
use std::fs;
pub struct Config {
    pub query: String,
    pub filename: String,
    pub case_sensitive: bool,
}

impl Config {
    pub fn new<T>(mut args: T) -> Result<Config, &'static str>
    where
        T: Iterator<Item = String>,
    {
        args.next();

        let query = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get a query string"),
        };

        let filename = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get a file name"),
        };

        //if is_err() - ENV variable is unset - proceed with sensitive case handling
        let case_sensitive = env::var("CASE_INSENSITIVE").is_err();

        Ok(Config {
            query,
            filename,
            case_sensitive,
        })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;

    let results = if config.case_sensitive {
        search(&config.query, &contents)
    } else {
        search_case_insensitive(&config.query, &contents)
    };
    for line in results {
        println!("{}", line);
    }
    Ok(())
}

pub fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    contents
        .lines()
        .filter(|line| line.contains(query))
        .collect()
}

pub fn search_case_insensitive<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let query = query.to_lowercase();
    contents
        .lines()
        .filter(|line| line.to_lowercase().contains(&query))
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn config_new_ok() {
        let args = ["program_name", "query", "filename.txt"]
            .iter()
            .map(|s| s.to_string());
        let config = Config::new(args).unwrap();
        assert_eq!(config.query, "query");
        assert_eq!(config.filename, "filename.txt");
    }

    #[test]
    #[should_panic(expected = "Didn't get a query string")]
    fn config_new_too_less_arguments() {
        let args = ["query"].iter().map(|s| s.to_string());
        let config = Config::new(args).unwrap();
        assert_eq!(config.query, "query");
        assert_eq!(config.filename, "filename.txt");
    }

    #[test]
    fn search_case_sensitive_no_result() {
        let query = "paul";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Duct tape.";
        assert!(search(query, contents).is_empty());
    }

    #[test]
    fn search_case_sensitive_one_result() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Duct tape.";

        assert_eq!(vec!["safe, fast, productive."], search(query, contents));
    }

    #[test]
    fn search_case_sensitive_multiple_result() {
        let query = "t";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Duct tape.";

        assert_eq!(
            vec![
                "Rust:",
                "safe, fast, productive.",
                "Pick three.",
                "Duct tape."
            ],
            search(query, contents)
        );
    }

    #[test]
    fn search_case_insensitive_multiple_result() {
        let query = "rUsT";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Trust me.";

        assert_eq!(
            vec!["Rust:", "Trust me."],
            search_case_insensitive(query, contents)
        );
    }
}
