use std::fs::File;
use std::io::ErrorKind;
use std::io::{self, Read};

fn main() {
    // call panic yourself
    // panic!("crash and burn");
    let v = vec![1, 2, 3];

    // will fail, inspect backtrace via 'RUST_BACKTRACE=1 cargo run'
    v[2];

    let f = File::open("hello.txt");

    let _f = match f {
        Ok(file) => file,
        Err(error) => match error.kind() {
            ErrorKind::NotFound => match File::create("hello.txt") {
                Ok(fc) => fc,
                Err(e) => panic!("Problem creating the file: {:?}", e),
            },
            other_error => {
                panic!("Problem opening the file: {:?}", other_error)
            }
        },
    };

    // better version for later
    // let f = File::open("hello.txt").unwrap_or_else(|error| {
    //     if error.kind() == ErrorKind::NotFound {
    //         File::create("hello.txt").unwrap_or_else(|error| {
    //             panic!("Problem creating the file: {:?}", error);
    //         })
    //     } else {
    //         panic!("Problem opening the file: {:?}", error);
    //     }
    // });
    //let f = File::open("hello2.txt").unwrap(); // unwrap is short for this, if OK, return value otherwise panic with error
    //let f = File::open("hello3.txt").expect("Failed to open hello3.txt"); // expect same as unwrap, but custom panic message
}

// Function for error propagation
fn _read_username_from_file() -> Result<String, io::Error> {
    let f = File::open("hello.txt");

    let mut f = match f {
        Ok(file) => file,
        Err(e) => return Err(e),
    };

    let mut s = String::new();

    match f.read_to_string(&mut s) {
        Ok(_) => Ok(s),
        Err(e) => Err(e),
    }
}

// shorted version via ? operator, that does pretty much the same as above, but does also convert the occuring error into the return type error via FROM trait
// fn read_username_from_file() -> Result<String, io::Error> {
//     let mut f = File::open("hello.txt")?;
//     let mut s = String::new();
//     f.read_to_string(&mut s)?;
//     Ok(s)
// }

// ? operator can also be chained
// fn read_username_from_file() -> Result<String, io::Error> {
//     let mut s = String::new();

//     File::open("hello.txt")?.read_to_string(&mut s)?;

//     Ok(s)
// }

// or build in function
// fn read_username_from_file() -> Result<String, io::Error> {
//     fs::read_to_string("hello.txt")
// }
