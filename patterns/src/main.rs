fn main() {
    // match arms
    let x = 1;

    match x {
        1 | 2 => println!("one or two"),
        3 => println!("three"),
        _ => println!("anything"),
    }

    let x = 5;

    // range only supported for char or numbers
    match x {
        1..=5 => println!("one through five"), // 1,2,3,4 or 5 will match
        _ => println!("something else"),
    }

    let x = 'c';

    match x {
        'a'..='j' => println!("early ASCII letter"),
        'k'..='z' => println!("late ASCII letter"),
        _ => println!("something else"),
    }

    // if let
    let favorite_color: Option<&str> = None;
    let is_tuesday = false;
    let age: Result<u8, _> = "12".parse();

    if let Some(color) = favorite_color {
        // will always run, if Some(color)
        println!("Using your favorite color, {}, as the background", color);
    } else if is_tuesday {
        // will only run, if favorite_color == None
        println!("Tuesday is green day!");
    } else if let Ok(age) = age {
        if age > 30 {
            println!("Using purple as the background color");
        } else {
            println!("Using orange as the background color");
        }
    } else {
        println!("Using blue as the background color");
    }

    // while let
    let mut stack = Vec::new();

    stack.push(1);
    stack.push(2);
    stack.push(3);

    while let Some(top) = stack.pop() {
        println!("{}", top);
    }

    // for loop (deconstruction of tuples)
    let v = vec!['a', 'b', 'c'];

    for (index, value) in v.iter().enumerate() {
        println!("{} is at index {}", value, index);
    }

    // let
    let (_x, _y, _z) = (1, 2, 3);

    let point = (3, 5);
    print_coordinates(&point);

    let x = Some(5);
    let y = 10;

    match x {
        Some(50) => println!("Got 50"),
        Some(y) => println!("Matched, y = {:?}", y), // be careful with shadow assignments
        _ => println!("Default case, x = {:?}", x),
    }

    println!("at the end: x = {:?}, y = {:?}", x, y);

    let p = Point { x: 0, y: 7 };

    let Point { x: a, y: b } = p;
    let Point { x, y } = p; // shorter version, variables are now called x and y as the fields inside the struct
    assert_eq!(0, a);
    assert_eq!(7, b);
    assert_eq!(0, x);
    assert_eq!(7, y);

    // possiblity to match on specific literals inside the struct
    match p {
        Point { x, y: 0 } => println!("On the x axis at {}", x),
        Point { x: 0, y } => println!("On the y axis at {}", y),
        Point { x, y } => println!("On neither axis: ({}, {})", x, y),
    }

    let msg = Message::ChangeColor(Color::Hsv(0, 160, 255));
    // let msg = Message::Quit;
    // let msg = Message::Move { x: 1, y: 9 };
    // let msg = Message::Write("Hey THere".to_string());

    match msg {
        Message::_Quit => {
            println!("The Quit variant has no data to destructure.")
        }
        Message::_Move { x, y } => {
            println!("Move in the x direction {} and in the y direction {}", x, y);
        }
        Message::_Write(text) => println!("Text message: {}", text),
        Message::ChangeColor(Color::_Rgb(r, g, b)) => {
            println!("Change the color to red {}, green {}, and blue {}", r, g, b)
        }
        Message::ChangeColor(Color::Hsv(h, s, v)) => println!(
            "Change the color to hue {}, saturation {}, and value {}",
            h, s, v
        ),
    }
    let ((_feet, _inches), Point { x, y }) = ((3, 10), Point { x: 3, y: -10 });
    println!("x was {}, y was {}", x, y);

    foo(3, 4);

    let mut setting_value = Some(5);
    let new_setting_value = Some(10);

    // matches, as long as settings and new settings contain some value
    match (setting_value, new_setting_value) {
        (Some(_), Some(_)) => {
            println!("Can't overwrite an existing customized value");
        }
        _ => {
            setting_value = new_setting_value;
        }
    }

    println!("setting is {:?}", setting_value);

    let numbers = (2, 4, 8, 16, 32);

    match numbers {
        (first, _, third, _, fifth) => {
            println!("Some numbers: {}, {}, {}", first, third, fifth)
        }
    }

    // ignore remaining part via ..
    let origin = PointXYZ { x: 0, _y: 0, _z: 0 };

    match origin {
        PointXYZ { x, .. } => println!("x is {}", x),
    }

    let numbers = (2, 4, 8, 16, 32);

    match numbers {
        (first, .., last) => {
            println!("Some numbers: {}, {}", first, last);
        }
    }

    // match guards, additional checks, that also need to be true
    let num = Some(4);

    match num {
        Some(x) if x < 5 => println!("less than five: {}", x),
        Some(x) => println!("{}", x),
        None => (),
    }

    let x = Some(10);
    let y = 10;

    match x {
        Some(50) => println!("Got 50"),
        Some(n) if n == y => println!("Matched, n = {}", n),
        _ => println!("Default case, x = {:?}", x),
    }

    println!("at the end: x = {:?}, y = {}", x, y);

    let x = 4;
    let y = false;

    match x {
        4 | 5 | 6 if y => println!("yes"), // match guard for 4 5 and 6 case
        _ => println!("no"),
    }

    let msg = MessageId::Hello { id: 5 };

    match msg {
        MessageId::Hello {
            id: id_variable @ 3..=7, // @ syntax, does binding and testing of variable at same time Hello(id) if id 3..=7
        } => println!("Found an id in range: {}", id_variable),
        //MessageId::Hello { id } if matches!(id, 3..=7) => println!("Found an id in range: {}", id), - is the same
        MessageId::Hello { id: 10..=12 } => {
            println!("Found an id in another range") // here, id is not a valid variable name (not defined)
        }
        MessageId::Hello { id } => println!("Found some other id: {}", id),
    }
}

// function
fn print_coordinates(&(x, y): &(i32, i32)) {
    println!("Current location: ({}, {})", x, y);
}

// struct destruction
struct Point {
    x: i32,
    y: i32,
}

// enum destruction
enum Message {
    _Quit,
    _Move { x: i32, y: i32 },
    _Write(String),
    ChangeColor(Color),
}

enum Color {
    _Rgb(i32, i32, i32),
    Hsv(i32, i32, i32),
}

fn foo(_: i32, y: i32) {
    println!("This code only uses the y parameter: {}", y);
}

struct PointXYZ {
    x: i32,
    _y: i32,
    _z: i32,
}

enum MessageId {
    Hello { id: i32 },
}
