#[derive(Debug)]
enum IpAddr {
    V4(u8, u8, u8, u8),
    V6(String),
}

#[derive(Debug)] // so we can inspect the state in a minute
enum UsState {
    _Alabama,
    Alaska,
    // --snip--
}
enum Coin {
    Penny,
    _Nickel,
    _Dime,
    Quarter(UsState),
}
impl Coin {
    fn value_in_cents(&self) -> u8 {
        match self {
            Coin::Penny => {
                println!("Lucky penny!");
                1
            }
            Coin::_Nickel => 5,
            Coin::_Dime => 10,
            Coin::Quarter(state) => {
                println!("State quarter from {:?}!", state);
                25
            }
        }
    }
}

fn main() {
    let home = IpAddr::V4(127, 0, 0, 1);

    let loopback = IpAddr::V6(String::from("::1"));
    println!("Home address {:?}", home);
    println!("loopback address {:?}", loopback);

    let _some_number = Some(5);
    let _some_string = Some("a string");

    let _absent_number: Option<i32> = None;

    println!("{}", Coin::Penny.value_in_cents());
    println!("{}", Coin::Quarter(UsState::Alaska).value_in_cents());

    let five = Some(5);
    println!("{}", five.unwrap());
    let six = plus_one(five);
    println!("{}", six.unwrap());
    let none = plus_one(None);
    println!("{}", none.is_none());

    // REWRITE this part via if-let statement
    // let config_max = Some(3u8);
    //  PATTERN
    // match config_max {
    //     FIRST_ARM
    //     Some(max) => println!("The maximum is configured to be {}", max),
    //     _ => (),
    // }
    let config_max = Some(3u8);
    //Syntax: if let PATTERN = FIRST_ARM{}
    if let Some(max) = config_max {
        println!("The maximum is configured to be {}", max);
    }
}

fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        None => None,
        Some(i) => Some(i + 1),
        //_ => (),  ignore otherwise all other clause, can also be bound to a variable
    }
}
