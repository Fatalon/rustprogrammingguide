# RustProgrammingGuide

## Name

this project is just a collection of some running code examples taken while i read the book `The Rust Programming Language` available at `https://doc.rust-lang.org/book/`

## Overview

* Chapter 1 - Getting Started
    - `hello_world`
    - `hello_cargo`
* Chapter 2: - Programming a Guessing Game
    - `guessing_game`
* Chapter 3: - Common Programming Concepts
    - `variables`
* Chapter 4: - Understanding Ownership
    - `ownership`
* Chapter 5: - Using Structs to Structure Related Data
    - `structs`
* Chapter 6: - Enums and Pattern Matching
    - `enums`
* Chapter 7: - Managing Growing Projects with Packages, Crates and Modules
    - `restaurant`
* Chapter 8: - Common Collections
    - `collections`
* Chapter 9: - Error Handling
    - `errors`
* Chapter 10: - Generic Types, Traits and Lifetimes
    - `generics_traits`
* Chapter 11: - Writing Automated Tests
    - `adder`
* Chapter 12: - An I/O Project: Building a Command Line Program (grep)
    - `minigrep`
* Chapter 13: - Functional Language Features: Iterators and Closures
    - `functional`
* Chapter 14: - More about Cargo and Crates.io
    - `cargo2`
* Chapter 15: - Smart Pointers
    - `smart_pointer`
* Chapter 16: - Fearless Concurrency
    - `concurrency`
* Chapter 17: - Object Oriented Programming: Features of Rust
    - `oop`
    - `blog`
* Chapter 18: - Patterns and Matching
    - `patterns`
* Chapter 19: - Advanced Features (Unsafe Rust, Advanced Traits, Types, Functions, Macros)
    - `advanced`
    - `hello_macro`
* Chapter 20: - Final Project: Building a Multithreaded Web Server
    - `hello`
