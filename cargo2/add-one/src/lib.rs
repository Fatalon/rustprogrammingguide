//! # Cargo Crate for section 14 of general Rust Guideline book
//!
//! `cargov2` is a collection of utilities to make performing certain
//! calculations more convenient.
//!
//! /// Adds one to the number given.
//!
//! # Cargo Crate for section 14 of general Rust Guideline book
//!
//! `cargov2` is a collection of utilities to make performing certain
//! calculations more convenient.
//!
//! # Art
//!
//! A library for modeling artistic concepts.
///
/// # Examples
///
/// ```
/// let arg = 5;
/// let answer = add_one::add_one(arg);
///
/// assert_eq!(6, answer);
/// ```
/// further sections could be:
///
/// # Panics
///
/// The scenarios in which the function being documented could panic
///
/// # Errors
///
/// If the function returns a Result, describing the kinds of errors that might occur and what conditions might cause those errors
/// to be returned can be helpful to callers so they can write code to handle the different kinds of errors in different ways.
///
/// # Safety
///
/// If the function is unsafe to call (we discuss unsafety in Chapter 19),
/// there should be a section explaining why the function is unsafe and covering the invariants that the function expects callers to uphold.
///
pub fn add_one(x: i32) -> i32 {
    x + 1
}

pub use self::kinds::PrimaryColor;
pub use self::kinds::SecondaryColor;
pub use self::utils::mix;

pub mod kinds {
    /// The primary colors according to the RYB color model.
    pub enum PrimaryColor {
        Red,
        Yellow,
        Blue,
    }

    /// The secondary colors according to the RYB color model.
    pub enum SecondaryColor {
        Orange,
        Green,
        Purple,
    }
}

pub mod utils {
    use crate::kinds::*;

    /// Combines two primary colors in equal amounts to create
    /// a secondary color.
    pub fn mix(c1: PrimaryColor, c2: PrimaryColor) -> SecondaryColor {
        SecondaryColor::Orange
        // --snip--
    }
}

use rand;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        assert_eq!(3, add_one(2));
    }
}
