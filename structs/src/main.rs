struct User {
    _active: bool,
    username: String,
    email: String,
    _sign_in_count: u64,
}

//tupple structs, like unamed struct members
struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

//use method syntax
impl Rectangle {
    // '&self' short for 'self: &Self', Self type is alias for type of impl block (in this case Rectangle)
    // use '&mut self' for mutual functions, 'self' is rare, only use case is transforming old data into something new
    // -> old reference will get invalid
    fn area(&self) -> u32 {
        self.width * self.height
    }

    // typical getter syntax
    fn width(&self) -> u32 {
        self.width
    }

    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width >= other.width && self.height >= other.height
    }
    // static methods called via :: (mostly for construction)
    fn square(size: u32) -> Rectangle {
        Rectangle {
            width: size,
            height: size,
        }
    }
}

fn main() {
    let mut user = User {
        email: String::from("someone@mail.com"),
        username: String::from("Hero"),
        _active: true,
        _sign_in_count: 1,
    };
    let user2 = build_user(String::from("a@gmx.com"), String::from("A"));
    println!("Hello, world, {}!", user.username);
    println!("Hello {}!", user2.username);

    user.username = String::from("NoHero");
    println!("Hello, world, {}!", user.username);

    //struct update syntax, behaves similar to '=' -> resolves via Move instruction, user2 invalid now due to String
    let user3 = User {
        email: String::from("another@example.com"),
        ..user2
    };
    println!("Hello {}!", user3.email);

    let _black = Color(0, 0, 0);
    let _origin = Point(0, 0, 0);

    let scale = 2;
    let rect1 = Rectangle {
        width: dbg!(30 * scale),
        height: 50,
    };
    let (area, rect1) = area_old(rect1);
    println!("The area of the rectangle is {} square pixels.", area);

    println!("The rectangle has attributes: {:?} ", rect1);
    dbg!(&rect1);
    println!(
        "The area of the rectangle is {} square pixels for a width of {}",
        rect1.area(),
        rect1.width()
    );

    let rect1 = Rectangle {
        width: 30,
        height: 50,
    };
    let rect2 = Rectangle {
        width: 10,
        height: 40,
    };
    let rect3 = Rectangle {
        width: 60,
        height: 45,
    };
    let sq = Rectangle::square(5);
    println!("square rectangle {:?}", sq);

    println!("Can rect1 hold rect2? {}", rect1.can_hold(&rect2));
    println!("Can rect1 hold rect3? {}", rect1.can_hold(&rect3));
}

//field init shorthand
fn build_user(email: String, username: String) -> User {
    User {
        email,
        username,
        _active: true,
        _sign_in_count: 1,
    }
}

fn area_old(rectangle: Rectangle) -> (u32, Rectangle) {
    (rectangle.width * rectangle.height, rectangle)
}
