use std::collections::HashMap;

enum SpreadsheetCell {
    Int(i32),
    Float(f64),
    Text(String),
}

fn main() {
    // let v: Vec<i32> = Vec::new();
    let mut v = vec![1, 2, 3];
    v.push(5);
    v.push(6);
    println!("Hello, world!");

    let third: &i32 = &v[2];
    println!("The third element is {}", third);

    match v.get(2) {
        Some(third) => println!("The third element is '' {}", third),
        None => println!("There is no third element."),
    }

    for i in &mut v {
        *i += 50;
    }

    for i in &v {
        println!("{}", i);
    }

    // enums allow you to put different values inside a vector
    let _row = vec![
        SpreadsheetCell::Int(3),
        SpreadsheetCell::Text(String::from("blue")),
        SpreadsheetCell::Float(10.12),
    ];

    let data = "initial contents";

    let _s = data.to_string();

    // the method also works on a literal directly:
    let _s = "initial contents".to_string();

    // similar
    let _s = String::from("initial contents");

    // UTF8 Strings all valid
    let _hello = String::from("こんにちは");
    let _hello = String::from("你好");
    let _hello = String::from("Dobrý den");
    let _hello = String::from("Hello");

    // Extending Strings
    let mut s1 = String::from("foo");
    let s2 = "bar";
    s1.push_str(s2);
    println!("s1 is {}", s1);
    println!("s2 is {}", s2);
    let mut s = String::from("lo");
    s.push('l');
    println!("s is {}", s);

    // Concatenation
    let s1 = String::from("Hello, ");
    let s2 = String::from("world!");
    let s1 = s1 + &s2; // note s1 has been moved here and can no longer be used
    println!("s1 is {}", s1);

    let s1 = String::from("tic");
    let s2 = String::from("tac");
    let s3 = String::from("toe");

    let s = s1 + "-" + &s2 + "-" + &s3;
    println!("s is {}", s);

    let s1 = String::from("tic");
    // format takes no ownership
    let s = format!("{}-{}-{}", s1, s2, s3);
    println!("s with format! is {}", s);

    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    let teams = vec![String::from("Blue"), String::from("Yellow")];
    let initial_scores = vec![10, 50];

    let mut scores: HashMap<_, _> = teams.into_iter().zip(initial_scores.into_iter()).collect();
    let team_name = String::from("Blue");
    let score = scores.get(&team_name);
    println!("Score of team {} is {}", team_name, score.unwrap());

    for (key, value) in &scores {
        println!("{}: {}", key, value);
    }

    // Overriding insert
    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Blue"), 25);

    println!("{:?}", scores);

    scores.entry(String::from("Red")).or_insert(50);
    scores.entry(String::from("Blue")).or_insert(50);

    println!("{:?}", scores);

    let text = "hello world wonderful world";

    let mut map = HashMap::new();

    for word in text.split_whitespace() {
        *map.entry(word).or_insert(0) += 1;
    }

    println!("{:?}", map);
}
