const THREE_HOURS_IN_MINUTES: u32 = 60 * 3;
const THREE_HOURS_IN_SECONDS: u32 = 60 * THREE_HOURS_IN_MINUTES;
const DECIMAL: u32 = 98_222;
const HEX: u8 = 0xff;
const OCTAL: u8 = 0o77;
const BINARY: u8 = 0b1111_0000;
const BYTE: u8 = b'A';

fn main() {
    let mut x = 5;
    println!("The value of x is {}!", x);
    x = 6;
    println!("The value of x is {}!", x);
    println!("The value of const is {}!", THREE_HOURS_IN_SECONDS);

    let x = 5;

    let x = x + 1;

    {
        let x = x * 2;
        println!("The value of x in the inner scope is: {}", x);
    }

    println!("The value of x is: {}", x);

    let mut spaces = "   ";
    println!("The value of spaces is: {}!", spaces);
    spaces = "        ";
    println!("The value of spaces is: {}!", spaces);
    let spaces = spaces.len();
    println!("The value of spaces is: {}!", spaces);

    println!(
        "Dec: {}, Hex: {}, Oct: {}, Bin: {}, Byte: {}",
        DECIMAL, HEX, OCTAL, BINARY, BYTE
    );

    let tup = (500, 6.4, 1);

    let (_, y, _) = tup;

    println!("The value of y is: {}", y);

    let a: [i32; 5] = [1, 2, 3, 4, 5];
    println!("The value of y is: {}", a[2]);
    let a = [1; 5]; // [1, 1, 1, 1, 1]
    println!("The value of y is: {}", a[0]);

    another_function(5);
    print_labeled_measurement(5, 'h');

    let y = {
        let x = 3;
        x + 1
    };

    println!("The value of y is: {}", y);

    let x = plus_one(5);

    println!("The value of x is: {}", x);

    let condition = true;
    let number = if condition { 5 } else { 6 };

    println!("The value of number is: {}", number);

    let mut counter = 0;

    let result = loop {
        counter += 1;

        if counter == 10 {
            break counter * 2;
        }
    };

    println!("The result is {}", result);

    for number in (1..4).rev() {
        println!("{}!", number);
    }
    println!("LIFTOFF!!!");
}

fn another_function(x: i32) {
    println!("Another function!");
    println!("The value of x is: {}", x);
}

fn print_labeled_measurement(value: i32, unit_label: char) {
    println!("The measurement is: {}{}", value, unit_label);
}

fn plus_one(x: i32) -> i32 {
    x + 1
}
